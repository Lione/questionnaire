-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2017-01-18 14:51:24
-- 服务器版本： 10.1.19-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `questionnaire`
--

-- --------------------------------------------------------

--
-- 表的结构 `test_manager`
--

CREATE TABLE `test_manager` (
  `admin` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `test_manager`
--

INSERT INTO `test_manager` (`admin`, `password`) VALUES
('admin', 'e10adc3949ba59abbe56e057f20f883e');

-- --------------------------------------------------------

--
-- 表的结构 `test_questionnaire`
--

CREATE TABLE `test_questionnaire` (
  `Code` varchar(100) NOT NULL,
  `ID` varchar(100) DEFAULT NULL,
  `OSA__Study_notes` varchar(100) DEFAULT NULL,
  `OSA__Study_involvement` varchar(100) DEFAULT NULL,
  `OSA__Diagnosis_Date` varchar(100) DEFAULT NULL,
  `OSA__ESS` varchar(100) DEFAULT NULL,
  `OSA__RDI` varchar(100) DEFAULT NULL,
  `OSA__TST` varchar(100) DEFAULT NULL,
  `OSA__Eff` varchar(100) DEFAULT NULL,
  `OSA__CPAP_titration` varchar(100) DEFAULT NULL,
  `OSA__CPAP_pressure` varchar(100) DEFAULT NULL,
  `OSA__CPAP_Compliance` varchar(100) DEFAULT NULL,
  `OSA__OA` varchar(100) DEFAULT NULL,
  `OSA__OA_compliance` varchar(100) DEFAULT NULL,
  `OSA__Others` varchar(100) DEFAULT NULL,
  `HTN__HTN` varchar(100) DEFAULT NULL,
  `HTN__Diagnosis_Date` varchar(100) DEFAULT NULL,
  `HTN__Medication` varchar(100) DEFAULT NULL,
  `HTN__Num_of_Antihypertensive_Agents` varchar(100) DEFAULT NULL,
  `HTN__Med1` varchar(100) DEFAULT NULL,
  `HTN__Med2` varchar(100) DEFAULT NULL,
  `HTN__Med3` varchar(100) DEFAULT NULL,
  `HTN__Med4` varchar(100) DEFAULT NULL,
  `HTN__Med5` varchar(100) DEFAULT NULL,
  `HTN__Notes` varchar(100) DEFAULT NULL,
  `DM__DM` varchar(100) DEFAULT NULL,
  `DM__IGT` varchar(100) DEFAULT NULL,
  `DM__IFG` varchar(100) DEFAULT NULL,
  `DM__Diagnosis_Date` varchar(100) DEFAULT NULL,
  `DM__Notes` varchar(100) DEFAULT NULL,
  `Hyperlipidaemia__Lipid` varchar(100) DEFAULT NULL,
  `Hyperlipidaemia__Diagnosis_Date` varchar(100) DEFAULT NULL,
  `Hyperlipidaemia__Med` varchar(100) DEFAULT NULL,
  `Hyperlipidaemia__Date_of_treatment` varchar(100) DEFAULT NULL,
  `Hyperlipidaemia__Notes` varchar(100) DEFAULT NULL,
  `NAFLD__NAFLD` varchar(100) DEFAULT NULL,
  `NAFLD__Diagnosis_Date` varchar(100) DEFAULT NULL,
  `NAFLD__Notes` varchar(100) DEFAULT NULL,
  `Metabolic_syndrome__MS` varchar(100) DEFAULT NULL,
  `Metabolic_syndrome__Diagnosis_Date` varchar(100) DEFAULT NULL,
  `Metabolic_syndrome__Notes` varchar(100) DEFAULT NULL,
  `Dysrrhythmias__Dysrrhythmias` varchar(100) DEFAULT NULL,
  `Dysrrhythmias__Atrial_fibrillation` varchar(100) DEFAULT NULL,
  `Dysrrhythmias__Diagnosis_Date` varchar(100) DEFAULT NULL,
  `Dysrrhythmias__Notes` varchar(100) DEFAULT NULL,
  `IHD__IHD` varchar(100) DEFAULT NULL,
  `IHD__AMI` varchar(100) DEFAULT NULL,
  `IHD__STEMI` varchar(100) DEFAULT NULL,
  `IHD__NSTEMI` varchar(100) DEFAULT NULL,
  `IHD__Diagnosis_Date` varchar(100) DEFAULT NULL,
  `IHD__Unstable_angina` varchar(100) DEFAULT NULL,
  `IHD__CABG` varchar(100) DEFAULT NULL,
  `IHD__PCI` varchar(100) DEFAULT NULL,
  `IHD__Date_of_intervention` varchar(100) DEFAULT NULL,
  `IHD__Notes` varchar(100) DEFAULT NULL,
  `Cardiomypathy__Cardiomypathy` varchar(100) DEFAULT NULL,
  `Cardiomypathy__Diagnosis_Date` varchar(100) DEFAULT NULL,
  `Cardiomypathy__Notes` varchar(100) DEFAULT NULL,
  `CHF__CHF` varchar(100) DEFAULT NULL,
  `CHF__LVEF` varchar(100) DEFAULT NULL,
  `CHF__Diagnosis_Date` varchar(100) DEFAULT NULL,
  `CHF__Notes` varchar(100) DEFAULT NULL,
  `Cor_pulmonale__Cor_pulmonale` varchar(100) DEFAULT NULL,
  `Cor_pulmonale__Diagnosis_Date` varchar(100) DEFAULT NULL,
  `Cor_pulmonale__Notes` varchar(100) DEFAULT NULL,
  `Carotid_Artery_Stenosis__Stenosis` varchar(100) DEFAULT NULL,
  `Carotid_Artery_Stenosis__Diagnosis_Date` varchar(100) DEFAULT NULL,
  `Carotid_Artery_Stenosis__Notes` varchar(100) DEFAULT NULL,
  `Stroke__Stroke` varchar(100) DEFAULT NULL,
  `Stroke__ICH` varchar(100) DEFAULT NULL,
  `Stroke__Cerebral_infarction` varchar(100) DEFAULT NULL,
  `Stroke__TIA` varchar(100) DEFAULT NULL,
  `Stroke__Diagnosis_Date` varchar(100) DEFAULT NULL,
  `Stroke__Notes` varchar(100) DEFAULT NULL,
  `Cancer__Cancer` varchar(100) DEFAULT NULL,
  `Cancer__Diagnosis` varchar(100) DEFAULT NULL,
  `Cancer__Pathology` varchar(100) DEFAULT NULL,
  `Cancer__Diagnosis_Date` varchar(100) DEFAULT NULL,
  `Cancer__Notes` varchar(100) DEFAULT NULL,
  `Mental_disorder__Mental_disorder` varchar(100) DEFAULT NULL,
  `Mental_disorder__Diagnosis` varchar(100) DEFAULT NULL,
  `Mental_disorder__Diagnosis_Date` varchar(100) DEFAULT NULL,
  `Mental_disorder__Notes` varchar(100) DEFAULT NULL,
  `COPD__COPD` varchar(100) DEFAULT NULL,
  `COPD__GOLD_Stage` varchar(100) DEFAULT NULL,
  `COPD__Diagnosis_Date` varchar(100) DEFAULT NULL,
  `COPD__Notes` varchar(100) DEFAULT NULL,
  `Blood_test__Test_date1` varchar(100) DEFAULT NULL,
  `Blood_test__Chol` varchar(100) DEFAULT NULL,
  `Blood_test__LDL` varchar(100) DEFAULT NULL,
  `Blood_test__HDL` varchar(100) DEFAULT NULL,
  `Blood_test__Trigl` varchar(100) DEFAULT NULL,
  `Blood_test__Test_date2` varchar(100) DEFAULT NULL,
  `Blood_test__HbA1c` varchar(100) DEFAULT NULL,
  `Blood_test__FS` varchar(100) DEFAULT NULL,
  `Mortality__Death` varchar(100) DEFAULT NULL,
  `Mortality__Direct_cause` varchar(100) DEFAULT NULL,
  `Mortality__Antecedent_causes` varchar(100) DEFAULT NULL,
  `Mortality__Date_of_death` varchar(100) DEFAULT NULL,
  `Mortality__Notes` varchar(100) NOT NULL,
  `OSA_related_disease__OSA_related_disease` varchar(100) DEFAULT NULL,
  `Censering_date` varchar(100) DEFAULT NULL,
  `Collecting_date` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `test_questionnaire`
--
ALTER TABLE `test_questionnaire`
  ADD PRIMARY KEY (`Code`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
