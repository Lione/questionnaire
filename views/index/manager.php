<?php
    use yii\helpers;
    use yii\widgets\ActiveForm;
?>
<style>
        *{
                font-family: 微软雅黑;
        }
        body{
                background-color: #666;
        }
        .Search{
                width: 405px;
                border: 2px solid #333;
                background-color: white;
                font-size: 20px;
                text-align: center;
        }
        .btn{
                width: 200px;
                border: 2px solid #333;
                background-color: white;
                font-size: 20px;
        }
        .btn:hover{
                background-color: #999;
        }
        .lists tr>th{
            width: 200px;
        }
        .lists{
            text-align: center;
            border:skyblue;
        }
        .lists tr:hover{
            background-color: #999;
        }
        .page_style .active a{
            background-color: #222;
            color: white;
        }
        .pagination{
            text-align: center;
        }
        .pagination li{
            list-style: none;
            display: inline;
            font-size: 18px;
        }
        .pagination a{
            text-decoration: none;
            padding: 5px;
            border: 1px solid #999;
        }
</style>
<body>
<div style="width: 1170px;background-color: white;padding: 15px;margin: 0 auto;box-shadow: -10px 10px #222">
<div style="border: 2px solid #333;padding: 15px;">
<table align="center">
<tr>
<td><a href="<?php echo helpers\Url::to(['index/questionnaire']);?>" target="_blank"><button class="btn">Add Questionnaire</button></a></td>
<td><a href="<?php echo helpers\Url::to(['index/manage']);?>"><button class="btn">Show All</button></a></td>
<td><a href="<?php echo helpers\Url::to(['index/export']);?>"><button class="btn">Export Excel</button></a></td>
<td><a href="<?php echo helpers\Url::to(['index/changepass']);?>"><button class="btn">Change Pass</button></a></td>
<td><a href="<?php echo helpers\Url::to(['index/logout']);?>"><button class="btn">Logout</button></a></td>
</tr>
<tr>
<td></td>
<td colspan="3">
    <form action="<?php echo helpers\Url::to(["index/search"]);?>" method="post">
    <input class="Search" type="text" name="search" placeholder="Code/ID">
    <input type="hidden"
    name="<?= \Yii::$app->request->csrfParam; ?>"
    value="<?= \Yii::$app->request->getCsrfToken();?>">
    <input class="btn" type="submit" value="Search"></form>
    </td>
</tr>
</table>
<div style="background-color: #B8CCE4">
<table class="lists" align="center" cellspacing="0"  border="1px solid">
<tr>
<th>NUM.</th>
<th>Code</th>
<th>ID</th>
<th>CollectTime</th>
<th colspan="3">Operate</th>
</tr>
<?php $i=1; foreach ($lists as $list):?>
<tr>
<td><?= $i++;?></td>
<td><?= $list['Code'];?></td>
<td><?= $list['ID'];?></td>
<td><?= $list['Collecting_date'];?></td>
<td><a class="view" href="<?= helpers\Url::to(["index/view",["Code"=>$list['Code']]]);?>">View</a></td>
<td><a class="edit" href="<?= helpers\Url::to(["index/edit",["Code"=>$list['Code']]]);?>">Edit</a></td>
<td><a class="del" href="<?= helpers\Url::to(["index/del",["Code"=>$list['Code']]]);?>" onclick="return confirm('[Warning]Can`t recovery after delete')">Del</a></td>
</tr>
<?php endforeach;?>
</table>
</div>
</div>
    <div class="pagination page_style">
    <?php echo yii\widgets\LinkPager::widget(['pagination' => $pager, 'prevPageLabel' => '&#8249;', 'nextPageLabel' => '&#8250;']); ?>
</div>
</div>
</body>
<script>
    <?php if(Yii::$app->session->hasFlash("info")){
        echo "alert("."'".Yii::$app->session->getFlash("info")."'".")";
    }?>
</script>
