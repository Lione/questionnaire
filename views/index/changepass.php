<?php
use yii\widgets\ActiveForm;
use yii\helpers;
?>
<style>
        *{
                font-family: 微软雅黑;
        }
        input{
                background-color: white;
                border: 2px solid #333;
                text-align: center;
        }
        tr>td:first-child{
                width:180px;
                text-align: center;
        }
        tr>td:last-child{
                text-align: center;
        }
        .btn:hover{
                background-color: #999;
        }
</style>
<body style="background-color: #666;">
<div style="margin: 0 auto;border: 2px solid #333;width: 500px;padding: 15px;background-color: white;height: 200px;box-shadow: -13px 13px #333;margin-top: 150px;">
<?php $form = ActiveForm::begin(["fieldConfig"=>["template"=>"{error}{input}"]])?>
<table align="center">
<tr>
<th colspan="2"><h1>Change Password</h1></th>
</tr>
<tr>
<td>OldPass:</td>
<td><?= $form->field($model,"OldPass")->passwordInput()?></td>
</tr>
<tr>
<td>NewPass:</td>
<td><?= $form->field($model, "NewPass")->passwordInput()?></td>
</tr>
<tr>
<td>NewPass-again:</td>
<td><?= $form->field($model, "RePass")->passwordInput()?></td>
</tr>
<tr>
<td><br><input class="btn" type="submit" value="Change"></td>
<td><br><input class="btn" type="reset" value="Reset"></td>
</tr>
</table>
<?php ActiveForm::end()?>
</div>
<footer>
<p style="color: white;position: fixed;bottom: 0px;">Version0.7--Build in 01/18/2017</p>
</footer>
<script>
    <?php if(Yii::$app->session->hasFlash("info")){
        echo "alert("."'".Yii::$app->session->getFlash("info")."'".")";
    }?>
</script>
</body>
