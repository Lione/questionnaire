<?php
$this->title = "Login";
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<style>
        *{
                font-family: 微软雅黑;
        }
        input{
                background-color: white;
                border: 2px solid #333;
                text-align: center;
                font-size: 16px;
        }
        tr>td:first-child{
                width:180px;
                text-align: center;
        }
        tr>td:last-child{
                text-align: center;
        }
        .btn:hover{
                background-color: #999;
        }
</style>
<body style="background-color: #666;">
<div style="margin: 0 auto;border: 2px solid #333;width: 500px;padding: 15px;background-color: white;height: 200px;box-shadow: -13px 13px #333;margin-top: 150px;">
<?php $form = ActiveForm::begin(["fieldConfig"=>["template"=>"{error}{input}"]])?>
<table align="center">
<tr>
<th colspan="2"><h1>Q system</h1></th>
</tr>
<tr>
<td>Admin:</td>
<td><?= $form->field($model, "admin")->textInput()?></td>
</tr>
<tr>
<td>Password:</td>
<td><?= $form->field($model, "password")->passwordInput()?></td>
</tr>
<tr>
<td><br><input type="submit" value="Login" class="btn"></td>
<td><br><input type="reset" value="Reset" class="btn"></td>
</tr>
</table>
<?php ActiveForm::end()?>
</div>
<footer>
<p style="color: white;position: fixed;bottom: 0px;">Version0.7--Build in 01/18/2017</p>
</footer>
<script>
    <?php if(Yii::$app->session->hasFlash("info")){
        echo "alert("."'".Yii::$app->session->getFlash("info")."'".")";
    }?>
</script>
</body>
