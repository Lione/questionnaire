<?php
$this->title = "Questionnaire";
use yii\helpers;
use yii\widgets\ActiveForm;
?>
<style>
*{
        font-family: 微软雅黑;
}
        input{
                text-align: center;
        }
        .inline{
        display: inline-block;
        vertical-align: top;
        width: 380px;
}
.inline>table{
        margin: 0 auto;
}
tr>td:first-child{
        text-align: right;
}
tr>td:last-child{
        text-align: left;
}
textarea{
        text-align: center;
        resize: none;
}
.lg{
        height: 400px;
}
.md{
        height: 280px;
}
.sm{
        height: 160px;
}
.btn{
        background-color: white;border-radius: 5px;font-size: 18px;
}
.btn:hover{
        background-color: #999;
}
</style>
<body style="background-color: #666">
<div style="width: 1170px;background-color: white;padding: 10px;text-align: center;margin: 0 auto;box-shadow: -13px 13px #333">
<?php $form = ActiveForm::begin([
    "options"=>['autocomplete'=>"off"],
    "fieldConfig"=>["template"=>"{error}{input}"]
    ])?>
<!-- 表头 -->
<div>
<table style="margin: 0 auto">
<tr>
<td>Code :</td>
<td><?= $form->field($model, "Code")->textInput(['autocomplete'=>"off"])?></td>
<td>&nbsp;&nbsp;&nbsp;</td>
<td>ID :</td>
<td><?= $form->field($model, "ID")->textInput()?></td>
</tr>
</table>
</div>
<!-- 结束 -->

<!-- 表一 -->
<div class="inline lg" style="background-color: #FFFEB2">
<table>
<tr>
<th colspan="2">OSA</th>
</tr>
<tr>
<td>Study notes :</td>
<td><?= $form->field($model, "OSA__Study_notes")->textInput()?></td>
</tr>
<tr>
<td>Study involvement :</td>
<td><?= $form->field($model, "OSA__Study_involvement")->textInput()?></td>
</tr>
<tr>
<td>Dignosis Date :</td>
<td><?= $form->field($model, "OSA__Diagnosis_Date")->textInput(["placeholder"=>"Day/Month/Year"])?></td>
</tr>
<tr>
<td>ESS :</td>
<td><?= $form->field($model, "OSA__ESS")->textInput()?></td>
</tr>
<tr>
<td>RDI :</td>
<td><?= $form->field($model, "OSA__RDI")->textInput()?></td>
</tr>
<tr>
<td>TST :</td>
<td><?= $form->field($model, "OSA__TST")->textInput()?></td>
</tr>	
<tr>
<td>Eff% :</td>
<td><?= $form->field($model, "OSA__Eff")->textInput()?></td>
</tr>
<tr>
<td>CPAP  titration:</td>
<td><?= $form->field($model, "OSA__CPAP_titration")->radioList(["Y"=>"Y","N"=>"N"])?></td>
</tr>
<tr>
<td>CPAP pressure :</td>
<td><?= $form->field($model, "OSA__CPAP_pressure")->textInput()?></td>
</tr>
<tr>
<td>CPAP Compliance :</td>
<td style="font-size:12px;"><?= $form->field($model,"OSA__CPAP_Compliance")->radioList(["G"=>"G","F"=>"F","N"=>"N","Loss to FU"=>"Loss to FU"])?></td>
</tr>		
<tr>
<td>OA :</td>
<td><?= $form->field($model, "OSA__OA")->radioList(["Y"=>"Y","N"=>"N"])?></td>
</tr>
<tr>
<td>OA compliance :</td>
<td style="font-size:12px;"><?= $form->field($model,"OSA__OA_compliance")->radioList(["G"=>"G","F"=>"F","N"=>"N","Loss to FU"=>"Loss to FU"])?></td>
</tr>				
<tr>
<td>Others :</td>
<td><?= $form->field($model, "OSA__Others")->textInput()?></td>
</tr>					
</table>
</div>
<!-- 结束 -->

<!-- 表二 -->
<div class="inline lg" id="HTN" style="background-color: #DEDAC4;">
<table>
<tr>
<th colspan="2">HTN</th>
</tr>
<tr>
<td>HTN :</td>
<td><?= $form->field($model, "HTN__HTN")->radioList(["Y"=>"Y","N"=>"N"],["id"=>"HTN__HTN","onclick"=>"special('Questionnaire[HTN__HTN]','HTN','HTN__HTN')"])?></td>
</tr>
<tr>
<td>Diagnosis Date :</td>
<td><?= $form->field($model, "HTN__Diagnosis_Date")->textInput(["placeholder"=>"Day/Month/Year"])?></td>
</tr>
<tr>
<td>Medication :</td>
<td><?= $form->field($model, "HTN__Medication")->radioList(["Y"=>"Y","N"=>"N"])?></td>
</tr>
<tr>
<td>Num. of Agents :</td>
<td><?= $form->field($model, "HTN__Num_of_Antihypertensive_Agents")->textInput()?></td>
</tr>
<tr>
<td>Med1 :</td>
<td><?= $form->field($model, "HTN__Med1")->textInput()?></td>
</tr>
<tr>
<td>Med2 :</td>
<td><?= $form->field($model, "HTN__Med2")->textInput()?></td>
</tr>
<tr>
<td>Med3 :</td>
<td><?= $form->field($model, "HTN__Med3")->textInput()?></td>
</tr>
<tr>
<td>Med4 :</td>
<td><?= $form->field($model, "HTN__Med4")->textInput()?></td>
</tr>
<tr>
<td>Med5 :</td>
<td><?= $form->field($model, "HTN__Med5")->textInput()?></td>
</tr>
<tr>
<td>Notes :</td>
<td><?= $form->field($model, "HTN__Notes")->textInput()?></td>
</tr>
</table>
</div>
<!-- 结束 -->

<!-- 表三 -->
<div class="inline lg" id="IHD" style="background-color: #8DB4E3;">
<table>
<tr>
<th colspan="2">IHD</th>
</tr>
<tr>
<td>IHD :</td>
<td><?= $form->field($model, "IHD__IHD")->radioList(["Y"=>"Y","N"=>"N"],["id"=>"IHD__IHD","onclick"=>"special('Questionnaire[IHD__IHD]','IHD','IHD__IHD')"])?></td>
</tr>
<tr>
<td>AMI :</td>
<td><?= $form->field($model, "IHD__AMI")->radioList(["Y"=>"Y","N"=>"N"])?></td>
</tr>
<tr>
<td>STEMI :</td>
<td><?= $form->field($model, "IHD__STEMI")->radioList(["Y"=>"Y","N"=>"N"])?></td>
</tr>
<tr>
<td>NSTEMI :</td>
<td><?= $form->field($model, "IHD__NSTEMI")->radioList(["Y"=>"Y","N"=>"N"])?></td>
</tr>
<tr>
<td>Unstable angina :</td>
<td><?= $form->field($model, "IHD__Unstable_angina")->radioList(["Y"=>"Y","N"=>"N"])?></td>
</tr>
<tr>
<td>Diagnosis Date :</td>
<td><?= $form->field($model, "IHD__Diagnosis_Date")->textarea(["placeholder"=>"Day/Month/Year","rows"=>3])?></td>
</tr>
<tr>
<td>CABG :</td>
<td><?= $form->field($model, "IHD__CABG")->radioList(["Y"=>"Y","N"=>"N"])?></td>
</tr>
<tr>
<td>PCI :</td>
<td><?= $form->field($model, "IHD__PCI")->radioList(["Y"=>"Y","N"=>"N"])?></td>
</tr>
<tr>
<td>Date of intervention :</td>
<td><?= $form->field($model, "IHD__Date_of_intervention")->textarea(["placeholder"=>"Day/Month/Year","rows"=>3])?></td>
</tr>
<tr>
<td>Notes :</td>
<td><?= $form->field($model, "IHD__Notes")->textInput()?></td>
</tr>
</table>
</div>
<!-- 结束 -->

<br><br>
<!-- 表4 -->
<div class="inline sm" id="DM" style="background-color: #B8CCE4;">
<table>
<tr>
<th colspan="2">DM</th>
</tr>
<tr>
<td>DM :</td>
<td><?= $form->field($model, "DM__DM")->radioList(["Y"=>"Y","N"=>"N"],["id"=>"DM__DM","onclick"=>"special('Questionnaire[DM__DM]','DM','DM__DM')"])?></td>
</tr>
<tr>
<td>IGT :</td>
<td><?= $form->field($model, "DM__IGT")->radioList(["Y"=>"Y","N"=>"N"])?></td>
</tr>
<tr>
<td>IFG :</td>
<td><?= $form->field($model, "DM__IFG")->radioList(["Y"=>"Y","N"=>"N"])?></td>
</tr>
<tr>
<td>Diagnosis Date :</td>
<td><?= $form->field($model, "DM__Diagnosis_Date")->textInput(["placeholder"=>"Day/Month/Year"])?></td>
</tr>
<tr>
<td>Notes :</td>
<td><?= $form->field($model, "DM__Notes")->textInput()?></td>
</tr>
</table>
</div>
<!-- 结束 -->

<!-- 表5 -->
<div class="inline sm" id="Lipid" style="background-color: #FFFEB2;">
<table>
<tr>
<th colspan="2">Hyperlipidaemia</th>
</tr>
<tr>
<td>Lipid :</td>
<td><?= $form->field($model, "Hyperlipidaemia__Lipid")->radioList(["Y"=>"Y","N"=>"N"],["id"=>"Hyperlipidaemia__Lipid","onclick"=>"special('Questionnaire[Hyperlipidaemia__Lipid]','Lipid','Hyperlipidaemia__Lipid')"])?></td>
</tr>
<tr>
<td>Diagnosis Date :</td>
<td><?= $form->field($model, "Hyperlipidaemia__Diagnosis_Date")->textInput(["placeholder"=>"Day/Month/Year"])?></td>
</tr>
<tr>
<td>Med :</td>
<td><?= $form->field($model, "Hyperlipidaemia__Med")->textInput()?></td>
</tr>
<tr>
<td>Date of treatment :</td>
<td><?= $form->field($model, "Hyperlipidaemia__Date_of_treatment")->textInput(["placeholder"=>"Day/Month/Year"])?></td>
</tr>
<tr>
<td>Notes :</td>
<td><?= $form->field($model, "Hyperlipidaemia__Notes")->textInput()?></td>
</tr>
</table>
</div>
<!-- 结束 -->

<!-- 表6 -->
<div class="inline sm" id="CHF" style="background-color: #C6F5FF;">
<table>
<tr>
<th colspan="2">CHF</th>
</tr>
<tr>
<td>CHF:</td>
<td><?= $form->field($model, "CHF__CHF")->radioList(["Y"=>"Y","N"=>"N"],["id"=>"CHF__CHF","onclick"=>"special('Questionnaire[CHF__CHF]','CHF','CHF__CHF')"])?></td>
</tr>
<tr>
<td>LVEF% :</td>
<td><?= $form->field($model, "CHF__LVEF")->textInput()?></td>
</tr>
<tr>
<td>Diagnosis Date :</td>
<td><?= $form->field($model, "CHF__Diagnosis_Date")->textarea(["placeholder"=>"Day/Month/Year","rows"=>3])?></td>
</tr>
<tr>
<td>Notes :</td>
<td><?= $form->field($model, "CHF__Notes")->textInput()?></td>
</tr>
</table>
</div>
<!-- 结束 -->

<br><br>

<!-- 表7 -->
<div class="inline sm" id="NAFLD" style="background-color: #FDEAD9;">
<table>
<tr>
<th colspan="2">NAFLD</th>
</tr>
<tr>
<td>NAFLD :</td>
<td><?= $form->field($model, "NAFLD__NAFLD")->radioList(["Y"=>"Y","N"=>"N"],["id"=>"NAFLD__NAFLD","onclick"=>"special('Questionnaire[NAFLD__NAFLD]','NAFLD','NAFLD__NAFLD')"])?></td>
</tr>
<tr>
<td>Diagnosis Date :</td>
<td><?= $form->field($model, "NAFLD__Diagnosis_Date")->textInput(["placeholder"=>"Day/Month/Year"])?></td>
</tr>
<tr>
<td>Notes :</td>
<td><?= $form->field($model, "NAFLD__Notes")->textInput()?></td>
</tr>
</table>
</div>
<!-- 结束 -->

<!-- 表8 -->
<div class="inline sm" id="MS" style="background-color: #DCFDCE;">
<table>
<tr>
<th colspan="2">Metabolic syndrome</th>
</tr>
<tr>
<td>MS :</td>
<td><?= $form->field($model, "Metabolic_syndrome__MS")->radioList(["Y"=>"Y","N"=>"N"],["id"=>"Metabolic_syndrome__MS","onclick"=>"special('Questionnaire[Metabolic_syndrome__MS]','MS','Metabolic_syndrome__MS')"])?></td>
</tr>
<tr>
<td>Diagnosis Date :</td>
<td><?= $form->field($model, "Metabolic_syndrome__Diagnosis_Date")->textInput(["placeholder"=>"Day/Month/Year"])?></td>
</tr>
<tr>
<td>Notes :</td>
<td><?= $form->field($model, "Metabolic_syndrome__Notes")->textInput()?></td>
</tr>
</table>
</div>
<!-- 结束 -->

<!-- 表9 -->
<div class="inline sm" id="Dysrrhythmias" style="background-color: #EDE4FF;">
<table>
<tr>
<th colspan="2">Dysrrhythmias</th>
</tr>
<tr>
<td>Dysrrhythmias :</td>
<td><?= $form->field($model, "Dysrrhythmias__Dysrrhythmias")->radioList(["Y"=>"Y","N"=>"N"],["id"=>"Dysrrhythmias__Dysrrhythmias","onclick"=>"special('Questionnaire[Dysrrhythmias__Dysrrhythmias]','Dysrrhythmias','Dysrrhythmias__Dysrrhythmias')"])?></td>
</tr>
<tr>
<td>Atrial fibrillation :</td>
<td><?= $form->field($model, "Dysrrhythmias__Atrial_fibrillation")->radioList(["Y"=>"Y","N"=>"N"])?></td>
</tr>
<tr>
<td>Diagnosis Date :</td>
<td><?= $form->field($model, "Dysrrhythmias__Diagnosis_Date")->textInput(["placeholder"=>"Day/Month/Year"])?></td>
</tr>
<tr>
<td>Notes :</td>
<td><?= $form->field($model, "Dysrrhythmias__Notes")->textInput()?></td>
</tr>
</table>
</div>
<!-- 结束 -->

<br><br>

<!-- 表10 -->
<div class="inline sm" id="Cardiomypathy" style="background-color: #FDDFF4;">
<table>
<tr>
<th colspan="2">CardiomyPathy</th>
</tr>
<tr>
<td>CardiomyPathy :</td>
<td><?= $form->field($model, "Cardiomypathy__Cardiomypathy")->radioList(["Y"=>"Y","N"=>"N"],["id"=>"Cardiomypathy__Cardiomypathy","onclick"=>"special('Questionnaire[Cardiomypathy__Cardiomypathy]','Cardiomypathy','Cardiomypathy__Cardiomypathy')"])?></td>
</tr>
<tr>
<td>Diagnosis Date :</td>
<td><?= $form->field($model, "Cardiomypathy__Diagnosis_Date")->textInput(["placeholder"=>"Day/Month/Year"])?></td>
</tr>
<tr>
<td>Notes :</td>
<td><?= $form->field($model, "Cardiomypathy__Notes")->textInput()?></td>
</tr>
</table>
</div>
<!-- 结束 -->

<!-- 表11 -->
<div class="inline sm" id="Cor" style="background-color: #BEF0C4;">
<table>
<tr>
<th colspan="2">Cor pulmonale</th>
</tr>
<tr>
<td>Cor pulmonale:</td>
<td><?= $form->field($model, "Cor_pulmonale__Cor_pulmonale")->radioList(["Y"=>"Y","N"=>"N"],["id"=>"Cor_pulmonale__Cor_pulmonale","onclick"=>"special('Questionnaire[Cor_pulmonale__Cor_pulmonale]','Cor','Cor_pulmonale__Cor_pulmonale')"])?></td>
</tr>
<tr>
<td>Diagnosis Date :</td>
<td><?= $form->field($model, "Cor_pulmonale__Diagnosis_Date")->textInput(["placeholder"=>"Day/Month/Year"])?></td>
</tr>
<tr>
<td>Notes :</td>
<td><?= $form->field($model, "Cor_pulmonale__Notes")->textInput()?></td>
</tr>
</table>
</div>
<!-- 结束 -->

<!-- 表12 -->
<div class="inline sm" id="Stenosis" style="background-color: #DEDAC4;">
<table>
<tr>
<th colspan="2">Cartid Artery Stenosis</th>
</tr>
<tr>
<td>Stenosis :</td>
<td><?= $form->field($model, "Carotid_Artery_Stenosis__Stenosis")->radioList(["Y"=>"Y","N"=>"N"],["id"=>"Carotid_Artery_Stenosis__Stenosis","onclick"=>"special('Questionnaire[Carotid_Artery_Stenosis__Stenosis]','Stenosis','Carotid_Artery_Stenosis__Stenosis')"])?></td>
</tr>
<tr>
<td>Diagnosis Date :</td>
<td><?= $form->field($model, "Carotid_Artery_Stenosis__Diagnosis_Date")->textInput(["placeholder"=>"Day/Month/Year"])?></td>
</tr>
<tr>
<td>Notes :</td>
<td><?= $form->field($model, "Carotid_Artery_Stenosis__Notes")->textInput()?></td>
</tr>
</table>
</div>
<!-- 结束 -->
<br><br>
<!-- 表十三-->
<div class="inline sm" id="COPD" style="background-color: #F1DBDA;">
<table>
<tr>
<th colspan="2">COPD</th>
</tr>
<tr>
<td>COPD :</td>
<td><?= $form->field($model, "COPD__COPD")->radioList(["Y"=>"Y","N"=>"N"],["id"=>"COPD__COPD","onclick"=>"special('Questionnaire[COPD__COPD]','COPD','COPD__COPD')"])?></td>
</tr>
<tr>
<td>GOLD Stage :</td>
<td><?= $form->field($model, "COPD__GOLD_Stage")->textInput()?></td>
</tr>
<tr>
<td>Diagnosis Date :</td>
<td><?= $form->field($model, "COPD__Diagnosis_Date")->textInput(["placeholder"=>"Day/Month/Year"])?></td>
</tr>
<tr>
<td>Notes :</td>
<td><?= $form->field($model, "COPD__Notes")->textInput()?></td>
</tr>
</table>
</div>
<!-- 结束 -->

<!-- 表十四 -->
<div class="inline sm" id="Cancer" style="background-color: #C7FEE7;">
<table>
<tr>
<th colspan="2">Cancer</th>
</tr>
<tr>
<td>Cancer :</td>
<td><?= $form->field($model, "Cancer__Cancer")->radioList(["Y"=>"Y","N"=>"N"],["id"=>"Cancer__Cancer","onclick"=>"special('Questionnaire[Cancer__Cancer]','Cancer','Cancer__Cancer')"])?></td>
</tr>
<tr>
<td>Diagnosis :</td>
<td><?= $form->field($model, "Cancer__Diagnosis")->textInput()?></td>
</tr>
<tr>
<td>Pathology :</td>
<td><?= $form->field($model, "Cancer__Pathology")->textInput()?></td>
</tr>
<tr>
<td>Diagnosis Date :</td>
<td><?= $form->field($model, "Cancer__Diagnosis_Date")->textInput(["placeholder"=>"Day/Month/Year"])?></td>
</tr>
<tr>
<td>Notes :</td>
<td><?= $form->field($model, "Cancer__Notes")->textInput()?></td>
</tr>
</table>
</div>
<!-- 结束 -->

<!-- 表十五 -->
<div class="inline sm" id="Mental" style="background-color: #8DB4E2;">
<table>
<tr>
<th colspan="2">Mental disorder</th>
</tr>
<tr>
<td>Mental disorder :</td>
<td><?= $form->field($model, "Mental_disorder__Mental_disorder")->radioList(["Y"=>"Y","N"=>"N"],["id"=>"Mental_disorder__Mental_disorder","onclick"=>"special('Questionnaire[Mental_disorder__Mental_disorder]','Mental','Mental_disorder__Mental_disorder')"])?></td>
</tr>
<tr>
<td>Diagnosis :</td>
<td><?= $form->field($model, "Mental_disorder__Diagnosis")->textInput()?></td>
</tr>
<tr>
<td>Diagnosis Date :</td>
<td><?= $form->field($model, "Mental_disorder__Diagnosis_Date")->textInput(["placeholder"=>"Day/Month/Year"])?></td>
</tr>
<tr>
<td>Notes :</td>
<td><?= $form->field($model, "Mental_disorder__Notes")->textInput()?></td>
</tr>
</table>
</div>
<!-- 结束 -->
<br><br>
<!-- 表十六 -->
<div class="inline md" id="Stroke" style="background-color: #FBC08F;">
<table>
<tr>
<th colspan="2">Stroke</th>
</tr>
<tr>
<td>Stroke :</td>
<td><?= $form->field($model, "Stroke__Stroke")->radioList(["Y"=>"Y","N"=>"N"],["id"=>"Stroke__Stroke","onclick"=>"special('Questionnaire[Stroke__Stroke]','Stroke','Stroke__Stroke')"])?></td>
</tr>
<tr>
<td>ICH :</td>
<td><?= $form->field($model, "Stroke__ICH")->radioList(["Y"=>"Y","N"=>"N"])?></td>
</tr>
<tr>
<td>Cerebral infarction :</td>
<td><?= $form->field($model, "Stroke__Cerebral_infarction")->radioList(["Y"=>"Y","N"=>"N"])?></td>
</tr>
<tr>
<td>TIA :</td>
<td><?= $form->field($model, "Stroke__TIA")->radioList(["Y"=>"Y","N"=>"N"])?></td>
</tr>
<tr>
<td>Diagnosis Date :</td>
<td><?= $form->field($model, "Stroke__Diagnosis_Date")->textarea(["placeholder"=>"Day/Month/Year","rows"=>3])?></td>
</tr>
<tr>
<td>Notes :</td>
<td><?= $form->field($model, "Stroke__Notes")->textInput()?></td>
</tr>
</table>
</div>
<!-- 结束 -->

<!-- 表十七 -->
<div class="inline md" style="background-color: #FFFEB2;">
<table>
<tr>
<th colspan="2">Latest Blood Test</th>
</tr>
<tr>
<td>Test date :</td>
<td><?= $form->field($model, "Blood_test__Test_date1")->textInput(["placeholder"=>"Day/Month/Year"])?></td>
</tr>
<tr>
<td>Chol :</td>
<td><?= $form->field($model, "Blood_test__Chol")->textInput()?></td>
</tr>
<tr>
<td>LDL :</td>
<td><?= $form->field($model, "Blood_test__LDL")->textInput()?></td>
</tr>
<tr>
<td>HDL :</td>
<td><?= $form->field($model, "Blood_test__HDL")->textInput()?></td>
</tr>
<tr>
<td>Trigl :</td>
<td><?= $form->field($model, "Blood_test__Trigl")->textInput()?></td>
</tr>
<tr>
<td>Test date :</td>
<td><?= $form->field($model, "Blood_test__Test_date2")->textInput(["placeholder"=>"Day/Month/Year"])?></td>
</tr>
<tr>
<td>HbA1c :</td>
<td><?= $form->field($model, "Blood_test__HbA1c")->textInput()?></td>
</tr>
<tr>
<td>FS :</td>
<td><?= $form->field($model, "Blood_test__FS")->textInput()?></td>
</tr>
</table>
</div>
<!-- 结束 -->


<!-- 表十八 -->
<div class="inline md" style="background-color: #FFE2F7;">
<table id="Mortality">
<tr>
<th colspan="2">Mortality</th>
</tr>
<tr>
<td>Death :</td>
<td><?= $form->field($model, "Mortality__Death")->radioList(["Y"=>"Y","N"=>"N"],["id"=>"Mortality__Death","onclick"=>"special('Questionnaire[Mortality__Death]','Mortality','Mortality__Death')"])?></td>
</tr>
<tr>
<td>Direct cause :</td>
<td><?= $form->field($model, "Mortality__Direct_cause")->textInput()?></td>
</tr>
<tr>
<td>Antecedent causes :</td>
<td><?= $form->field($model, "Mortality__Antecedent_causes")->textInput()?></td>
</tr>
<tr>
<td>Date of Death :</td>
<td><?= $form->field($model, "Mortality__Date_of_death")->textInput(["placeholder"=>"Day/Month/Year"])?></td>
</tr>
<tr>
<td>Notes :</td>
<td><?= $form->field($model, "Mortality__Notes")->textInput()?></td>
</tr>
</table><br><br>
<!-- 表十九 -->
<div style="background-color: #FFFF00;">
<table>
<tr>
<th colspan="2">OSA-related disease</th>
</tr>
<tr>
<td>OSA-related disease :</td>
<td><?= $form->field($model, "OSA_related_disease__OSA_related_disease")->textInput()?></td>
</tr>
</table>
</div>
<!-- 结束 -->
</div>
<!-- 结束 -->


<br><br>
<!-- 表二十 -->
<div style="background-color: #8DB4E3;">
<table align="center">
<tr>
<td>Censering date:</td>
<td><?= $form->field($model, "Censering_date")->textInput(["placeholder"=>"Day/Month/Year"])?></td>
<td>Collecting date:</td>
<td><?= $form->field($model, "Collecting_date")->textInput(["placeholder"=>"Day/Month/Year"])?></td>
<td><input class="btn" type="submit" value="submit"></td>
</tr>
</table>
</div>
<!-- 结束 -->
<?php ActiveForm::end()?>
</div>
<footer>
    <div style="text-align:center">
        <a href="<?php echo helpers\Url::to(['index/login']);?>"><button class="btn">Manager</button></a>
    </div>
</footer>
<script src="./assets/jquery.min.js"></script>
<script>
    <?php if(Yii::$app->session->hasFlash("info")){
        echo "alert("."'".Yii::$app->session->getFlash("info")."'".");";
    }?>
        
function special(name,parent,select){
            var radio = document.getElementsByName(name);
            for(i=0;i<radio.length;i++){
                if(radio[i].checked){
                    if(radio[i].value=='N'){
                         $("#"+parent).find("input[type='text']").attr("readonly",true);
                         $('#'+parent).find("input:radio[value='Y']").prop("checked",false);
                         $('#'+parent).find("input:radio[value='N']").prop("checked","checked");
//                         $("#"+parent).find("input:radio").attr("disabled",true);
                         $("#"+parent).find("textarea").attr("readonly",true);
                         $("#"+parent).find("input[type='text']").css("backgroundColor","#333");
                         $("#"+parent).find("textarea").css("backgroundColor","#333");
                         $("#"+parent).find("input[type='text']").val(null);
                         $("#"+parent).find("textarea").val(null);
//                         $('#'+parent).find("input[type='radio']").prop('checked',false);
//                         $('#'+select).find("input:radio[name='Questionnaire[HTN__HTN]']").eq('N').prop("checked",true);
                     }else if(radio[i].value=='Y'){
                         $('#'+parent).find("input[type='text']").removeAttr("readonly"); 
                         $("#"+parent).find("input:radio").removeAttr("disabled");
                         $("#"+parent).find("textarea").removeAttr("readonly");
                         $("#"+parent).find("input[type='text']").css("backgroundColor","white");
                         $("#"+parent).find("textarea").css("backgroundColor","white");
                     }
                }
            }
            $("#questionnaire-htn__notes").css("backgroundColor","white");
            $("#questionnaire-htn__notes").removeAttr("readonly");
            $("#questionnaire-ihd__notes").css("backgroundColor","white");
            $("#questionnaire-ihd__notes").removeAttr("readonly");
            $("#questionnaire-hyperlipidaemia__notes").css("backgroundColor","white");
            $("#questionnaire-hyperlipidaemia__notes").removeAttr("readonly");
            $("#questionnaire-nafld__notes").css("backgroundColor","white");
            $("#questionnaire-nafld__notes").removeAttr("readonly");
            $("#questionnaire-metabolic_syndrome__notes").css("backgroundColor","white");
            $("#questionnaire-metabolic_syndrome__notes").removeAttr("readonly");
            $("#questionnaire-dysrrhythmias__notes").css("backgroundColor","white");
            $("#questionnaire-dysrrhythmias__notes").removeAttr("readonly");
            $("#questionnaire-dm__notes").css("backgroundColor","white");
            $("#questionnaire-dm__notes").removeAttr("readonly");
            $("#questionnaire-cardiomypathy__notes").css("backgroundColor","white");
            $("#questionnaire-cardiomypathy__notes").removeAttr("readonly");
            $("#questionnaire-chf__notes").css("backgroundColor","white");
            $("#questionnaire-chf__notes").removeAttr("readonly");
            $("#questionnaire-cor_pulmonale__notes").css("backgroundColor","white");
            $("#questionnaire-cor_pulmonale__notes").removeAttr("readonly");
            $("#questionnaire-carotid_artery_stenosis__notes").css("backgroundColor","white");
            $("#questionnaire-carotid_artery_stenosis__notes").removeAttr("readonly");
            $("#questionnaire-copd__notes").css("backgroundColor","white");
            $("#questionnaire-copd__notes").removeAttr("readonly");
            $("#questionnaire-cancer__notes").css("backgroundColor","white");
            $("#questionnaire-cancer__notes").removeAttr("readonly");
            $("#questionnaire-mental_disorder__notes").css("backgroundColor","white");
            $("#questionnaire-mental_disorder__notes").removeAttr("readonly");
            $("#questionnaire-stroke__notes").css("backgroundColor","white");
            $("#questionnaire-stroke__notes").removeAttr("readonly");
            $("#questionnaire-mortality__notes").css("backgroundColor","white");
            $("#questionnaire-mortality__notes").removeAttr("readonly");
    }
</script>
</body>

