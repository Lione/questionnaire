<?php

/* @var $this yii\web\View */

$this->title = 'Questionnaire';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="<?php echo Yii::$app->homeUrl?>">Get started with Yii</a></p>
    </div>

    <div class="body-content">
        </div>

    </div>
</div>
