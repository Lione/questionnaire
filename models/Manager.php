<?php
namespace app\models;
use Yii;
use yii\db\ActiveRecord;

class Manager extends ActiveRecord{
    
    public $OldPass = '';
    public $NewPass = '';
    public $RePass = '';


    public static function tableName() {
        return "{{%manager}}";
    }
    
    public function rules() {
        return [
            [["admin","password"],"required","on"=>"login"],
            [["OldPass","NewPass","RePass"],"required","on"=>"change"],
            ["RePass","compare","compareAttribute"=>"NewPass","message"=>"Password not compare","on"=>"change"]
            ];
    }
    
    public function login($data){
        $this->scenario = "login";
        if($this->load($data)&&$this->validate()){
            $result = self::find()->where("admin = :admin and password = :password",[":admin"=>$this->admin,"password"=> md5($this->password)])->one();
            if($result){
                $lifetime = 1*3600;
                $session = Yii::$app->session;
                session_set_cookie_params($lifetime);
                $session['admin'] = [
                    'adminname' => $this->admin,
                    'oldp' => md5($this->password),
                    'isLogin' => "808",
                ];
                return TRUE;
            }
            return FALSE;
        }
        return FALSE;
    }
    
    public function change($data){
        $this->scenario = "change";
        if($this->load($data)&&$this->validate()){
            if(MD5($this->OldPass) == Yii::$app->session["admin"]["oldp"]){
                $result = self::updateAll(["password"=> md5($this->NewPass)],"admin = :admin",[":admin"=>Yii::$app->session["admin"]["adminname"]]);
                if($result){
                    return true;
                }
                return false;
            }
            return false;
        }
        return false;
    }
}
