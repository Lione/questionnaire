<?php
namespace app\models;
use yii\db\ActiveRecord;

class Questionnaire extends ActiveRecord{
    public static function tableName() {
        return "{{%questionnaire}}";
    }
    
    public function rules() {
        return [
            [["Code","ID"],"required"]
        ];
    }
    
    function submit($data){
        if($this->load($data)&&$this->validate()){
            $this->OSA__Study_notes = $data["Questionnaire"]["OSA__Study_notes"];
            $this->OSA__Study_involvement = $data["Questionnaire"]["OSA__Study_involvement"];
            $this->OSA__Diagnosis_Date = $data["Questionnaire"]["OSA__Diagnosis_Date"];
            $this->OSA__ESS = $data["Questionnaire"]["OSA__ESS"];
            $this->OSA__RDI = $data["Questionnaire"]["OSA__RDI"];
            $this->OSA__TST = $data["Questionnaire"]["OSA__TST"];
            $this->OSA__Eff = $data["Questionnaire"]["OSA__Eff"];
            $this->OSA__CPAP_titration = $data["Questionnaire"]["OSA__CPAP_titration"];
            $this->OSA__CPAP_pressure = $data["Questionnaire"]["OSA__CPAP_pressure"];
            $this->OSA__CPAP_Compliance = $data["Questionnaire"]["OSA__CPAP_Compliance"];
            $this->OSA__OA = $data["Questionnaire"]["OSA__OA"];
            $this->OSA__OA_compliance = $data["Questionnaire"]["OSA__OA_compliance"];
            $this->OSA__Others = $data["Questionnaire"]["OSA__Others"];

            $this->HTN__HTN = $data["Questionnaire"]["HTN__HTN"];
            $this->HTN__Diagnosis_Date = $data["Questionnaire"]["HTN__Diagnosis_Date"];
            $this->HTN__Medication = $data["Questionnaire"]["HTN__Medication"];
            $this->HTN__Num_of_Antihypertensive_Agents = $data["Questionnaire"]["HTN__Num_of_Antihypertensive_Agents"];
            $this->HTN__Med1 = $data["Questionnaire"]["HTN__Med1"];
            $this->HTN__Med2 = $data["Questionnaire"]["HTN__Med2"];
            $this->HTN__Med3 = $data["Questionnaire"]["HTN__Med3"];
            $this->HTN__Med4 = $data["Questionnaire"]["HTN__Med4"];
            $this->HTN__Med5 = $data["Questionnaire"]["HTN__Med5"];
            $this->HTN__Notes = $data["Questionnaire"]["HTN__Notes"];

            $this->DM__DM = $data["Questionnaire"]["DM__DM"];
            $this->DM__IGT = $data["Questionnaire"]["DM__IGT"];
            $this->DM__IFG = $data["Questionnaire"]["DM__IFG"];
            $this->DM__Diagnosis_Date = $data["Questionnaire"]["DM__Diagnosis_Date"];
            $this->DM__Notes = $data["Questionnaire"]["DM__Notes"];

            $this->Hyperlipidaemia__Lipid = $data["Questionnaire"]["Hyperlipidaemia__Lipid"];
            $this->Hyperlipidaemia__Diagnosis_Date = $data["Questionnaire"]["Hyperlipidaemia__Diagnosis_Date"];
            $this->Hyperlipidaemia__Med = $data["Questionnaire"]["Hyperlipidaemia__Med"];
            $this->Hyperlipidaemia__Date_of_treatment = $data["Questionnaire"]["Hyperlipidaemia__Date_of_treatment"];
            $this->Hyperlipidaemia__Notes = $data["Questionnaire"]["Hyperlipidaemia__Notes"];
            
            $this->NAFLD__NAFLD = $data["Questionnaire"]["NAFLD__NAFLD"];
            $this->NAFLD__Diagnosis_Date = $data["Questionnaire"]["NAFLD__Diagnosis_Date"];
            $this->NAFLD__Notes = $data["Questionnaire"]["NAFLD__Notes"];
            
            $this->Metabolic_syndrome__MS = $data["Questionnaire"]["Metabolic_syndrome__MS"];
            $this->Metabolic_syndrome__Diagnosis_Date = $data["Questionnaire"]["Metabolic_syndrome__Diagnosis_Date"];
            $this->Metabolic_syndrome__Notes = $data["Questionnaire"]["Metabolic_syndrome__Notes"];
            
            $this->Dysrrhythmias__Dysrrhythmias = $data["Questionnaire"]["Dysrrhythmias__Dysrrhythmias"];
            $this->Dysrrhythmias__Atrial_fibrillation = $data["Questionnaire"]["Dysrrhythmias__Atrial_fibrillation"];
            $this->Dysrrhythmias__Diagnosis_Date = $data["Questionnaire"]["Dysrrhythmias__Diagnosis_Date"];
            $this->Dysrrhythmias__Notes = $data["Questionnaire"]["Dysrrhythmias__Notes"];
            
            $this->IHD__IHD = $data["Questionnaire"]["IHD__IHD"];
            $this->IHD__AMI = $data["Questionnaire"]["IHD__AMI"];
            $this->IHD__STEMI = $data["Questionnaire"]["IHD__STEMI"];
            $this->IHD__NSTEMI = $data["Questionnaire"]["IHD__NSTEMI"];
            $this->IHD__Diagnosis_Date = $data["Questionnaire"]["IHD__Diagnosis_Date"];
            $this->IHD__Unstable_angina = $data["Questionnaire"]["IHD__Unstable_angina"];
            $this->IHD__CABG = $data["Questionnaire"]["IHD__CABG"];
            $this->IHD__PCI = $data["Questionnaire"]["IHD__PCI"];
            $this->IHD__Date_of_intervention	= $data["Questionnaire"]["IHD__Date_of_intervention"];
            $this->IHD__Notes = $data["Questionnaire"]["IHD__Notes"];
            
            $this->Cardiomypathy__Cardiomypathy = $data["Questionnaire"]["Cardiomypathy__Cardiomypathy"];
            $this->Cardiomypathy__Diagnosis_Date = $data["Questionnaire"]["Cardiomypathy__Diagnosis_Date"];
            $this->Cardiomypathy__Notes = $data["Questionnaire"]["Cardiomypathy__Notes"];
            
            $this->CHF__CHF = $data["Questionnaire"]["CHF__CHF"];
            $this->CHF__LVEF = $data["Questionnaire"]["CHF__LVEF"];
            $this->CHF__Diagnosis_Date = $data["Questionnaire"]["CHF__Diagnosis_Date"];
            $this->CHF__Notes = $data["Questionnaire"]["CHF__Notes"];
            
            $this->Cor_pulmonale__Cor_pulmonale = $data["Questionnaire"]["Cor_pulmonale__Cor_pulmonale"];
            $this->Cor_pulmonale__Diagnosis_Date = $data["Questionnaire"]["Cor_pulmonale__Diagnosis_Date"];
            $this->Cor_pulmonale__Notes = $data["Questionnaire"]["Cor_pulmonale__Notes"];
            
            $this->Carotid_Artery_Stenosis__Stenosis = $data["Questionnaire"]["Carotid_Artery_Stenosis__Stenosis"];
            $this->Carotid_Artery_Stenosis__Diagnosis_Date = $data["Questionnaire"]["Carotid_Artery_Stenosis__Diagnosis_Date"];
            $this->Carotid_Artery_Stenosis__Notes = $data["Questionnaire"]["Carotid_Artery_Stenosis__Notes"];
            
            $this->Stroke__Stroke = $data["Questionnaire"]["Stroke__Stroke"];
            $this->Stroke__ICH = $data["Questionnaire"]["Stroke__ICH"];
            $this->Stroke__Cerebral_infarction = $data["Questionnaire"]["Stroke__Cerebral_infarction"];
            $this->Stroke__TIA = $data["Questionnaire"]["Stroke__TIA"];
            $this->Stroke__Diagnosis_Date = $data["Questionnaire"]["Stroke__Diagnosis_Date"];
            $this->Stroke__Notes = $data["Questionnaire"]["Stroke__Notes"];
            
            $this->Cancer__Cancer = $data["Questionnaire"]["Cancer__Cancer"];
            $this->Cancer__Diagnosis = $data["Questionnaire"]["Cancer__Diagnosis"];
            $this->Cancer__Pathology = $data["Questionnaire"]["Cancer__Pathology"];
            $this->Cancer__Diagnosis_Date = $data["Questionnaire"]["Cancer__Diagnosis_Date"];
            $this->Cancer__Notes = $data["Questionnaire"]["Cancer__Notes"];
            
            $this->Mental_disorder__Mental_disorder = $data["Questionnaire"]["Mental_disorder__Mental_disorder"];
            $this->Mental_disorder__Diagnosis = $data["Questionnaire"]["Mental_disorder__Diagnosis"];
            $this->Mental_disorder__Diagnosis_Date = $data["Questionnaire"]["Mental_disorder__Diagnosis_Date"];
            $this->Mental_disorder__Notes = $data["Questionnaire"]["Mental_disorder__Notes"];
            
            $this->COPD__COPD = $data["Questionnaire"]["COPD__COPD"];
            $this->COPD__GOLD_Stage = $data["Questionnaire"]["COPD__GOLD_Stage"];
            $this->COPD__Diagnosis_Date = $data["Questionnaire"]["COPD__Diagnosis_Date"];
            $this->COPD__Notes = $data["Questionnaire"]["COPD__Notes"];
            
            $this->Blood_test__Test_date1 = $data["Questionnaire"]["Blood_test__Test_date1"];
            $this->Blood_test__Chol = $data["Questionnaire"]["Blood_test__Chol"];
            $this->Blood_test__LDL = $data["Questionnaire"]["Blood_test__LDL"];
            $this->Blood_test__HDL = $data["Questionnaire"]["Blood_test__HDL"];
            $this->Blood_test__Trigl = $data["Questionnaire"]["Blood_test__Trigl"];
            $this->Blood_test__Test_date2 = $data["Questionnaire"]["Blood_test__Test_date2"];
            $this->Blood_test__HbA1c = $data["Questionnaire"]["Blood_test__HbA1c"];
            $this->Blood_test__FS = $data["Questionnaire"]["Blood_test__FS"];

            $this->Mortality__Death = $data["Questionnaire"]["Mortality__Death"];
            $this->Mortality__Direct_cause = $data["Questionnaire"]["Mortality__Direct_cause"];
            $this->Mortality__Antecedent_causes = $data["Questionnaire"]["Mortality__Antecedent_causes"];
            $this->Mortality__Date_of_death = $data["Questionnaire"]["Mortality__Date_of_death"];
            $this->Mortality__Notes = $data["Questionnaire"]["Mortality__Notes"];

            $this->OSA_related_disease__OSA_related_disease = $data["Questionnaire"]["OSA_related_disease__OSA_related_disease"];

            $this->Censering_date = $data["Questionnaire"]["Censering_date"];
            $this->Collecting_date = $data["Questionnaire"]["Collecting_date"];
            if($this->save(false)){
                return true;
            }
            return false;
        }
        return FALSE;
    }
    
    function modelUpdate($data){
        if($this->load($data)&&$this->validate()){
            if(self::updateAll([
            "OSA__Study_notes"=>$data["Questionnaire"]["OSA__Study_notes"],
            "OSA__Study_involvement"=>$data["Questionnaire"]["OSA__Study_involvement"],
            "OSA__Diagnosis_Date"=>$data["Questionnaire"]["OSA__Diagnosis_Date"],
            "OSA__ESS"=>$data["Questionnaire"]["OSA__ESS"],
            "OSA__RDI"=>$data["Questionnaire"]["OSA__RDI"],
            "OSA__TST"=>$data["Questionnaire"]["OSA__TST"],
            "OSA__Eff"=>$data["Questionnaire"]["OSA__Eff"],
            "OSA__CPAP_titration"=>$data["Questionnaire"]["OSA__CPAP_titration"],
            "OSA__CPAP_pressure"=>$data["Questionnaire"]["OSA__CPAP_pressure"],
            "OSA__CPAP_Compliance"=>$data["Questionnaire"]["OSA__CPAP_Compliance"],
            "OSA__OA"=>$data["Questionnaire"]["OSA__OA"],
            "OSA__OA_compliance"=>$data["Questionnaire"]["OSA__OA_compliance"],
            "OSA__Others"=>$data["Questionnaire"]["OSA__Others"],

            "HTN__HTN"=>$data["Questionnaire"]["HTN__HTN"],
            "HTN__Diagnosis_Date"=>$data["Questionnaire"]["HTN__Diagnosis_Date"],
            "HTN__Medication"=>$data["Questionnaire"]["HTN__Medication"],
            "HTN__Num_of_Antihypertensive_Agents"=>$data["Questionnaire"]["HTN__Num_of_Antihypertensive_Agents"],
            "HTN__Med1"=>$data["Questionnaire"]["HTN__Med1"],
            "HTN__Med2"=>$data["Questionnaire"]["HTN__Med2"],
            "HTN__Med3"=>$data["Questionnaire"]["HTN__Med3"],
            "HTN__Med4"=>$data["Questionnaire"]["HTN__Med4"],
            "HTN__Med5"=>$data["Questionnaire"]["HTN__Med5"],
            "HTN__Notes"=>$data["Questionnaire"]["HTN__Notes"],

            "DM__DM"=>$data["Questionnaire"]["DM__DM"],
            "DM__IGT"=>$data["Questionnaire"]["DM__IGT"],
            "DM__IFG"=>$data["Questionnaire"]["DM__IFG"],
            "DM__Diagnosis_Date"=>$data["Questionnaire"]["DM__Diagnosis_Date"],
            "DM__Notes"=>$data["Questionnaire"]["DM__Notes"],

            "Hyperlipidaemia__Lipid"=>$data["Questionnaire"]["Hyperlipidaemia__Lipid"],
            "Hyperlipidaemia__Diagnosis_Date"=>$data["Questionnaire"]["Hyperlipidaemia__Diagnosis_Date"],
            "Hyperlipidaemia__Med"=>$data["Questionnaire"]["Hyperlipidaemia__Med"],
            "Hyperlipidaemia__Date_of_treatment"=>$data["Questionnaire"]["Hyperlipidaemia__Date_of_treatment"],
            "Hyperlipidaemia__Notes"=>$data["Questionnaire"]["Hyperlipidaemia__Notes"],
                
            "NAFLD__NAFLD"=>$data["Questionnaire"]["NAFLD__NAFLD"],
            "NAFLD__Diagnosis_Date"=>$data["Questionnaire"]["NAFLD__Diagnosis_Date"],
            "NAFLD__Notes"=>$data["Questionnaire"]["NAFLD__Notes"],

            "Metabolic_syndrome__MS"=>$data["Questionnaire"]["Metabolic_syndrome__MS"],
            "Metabolic_syndrome__Diagnosis_Date"=>$data["Questionnaire"]["Metabolic_syndrome__Diagnosis_Date"],
            "Metabolic_syndrome__Notes"=>$data["Questionnaire"]["Metabolic_syndrome__Notes"],
                
            "Dysrrhythmias__Dysrrhythmias"=>$data["Questionnaire"]["Dysrrhythmias__Dysrrhythmias"],
            "Dysrrhythmias__Atrial_fibrillation"=>$data["Questionnaire"]["Dysrrhythmias__Atrial_fibrillation"],
            "Dysrrhythmias__Diagnosis_Date"=>$data["Questionnaire"]["Dysrrhythmias__Diagnosis_Date"],
            "Dysrrhythmias__Notes"=>$data["Questionnaire"]["Dysrrhythmias__Notes"],
                
            "IHD__IHD"=>$data["Questionnaire"]["IHD__IHD"],
            "IHD__AMI"=>$data["Questionnaire"]["IHD__AMI"],
            "IHD__STEMI"=>$data["Questionnaire"]["IHD__STEMI"],
            "IHD__NSTEMI"=>$data["Questionnaire"]["IHD__NSTEMI"],
            "IHD__Diagnosis_Date"=>$data["Questionnaire"]["IHD__Diagnosis_Date"],
            "IHD__Unstable_angina"=>$data["Questionnaire"]["IHD__Unstable_angina"],
            "IHD__CABG"=>$data["Questionnaire"]["IHD__CABG"],
            "IHD__PCI"=>$data["Questionnaire"]["IHD__PCI"],
            "IHD__Date_of_intervention" => $data["Questionnaire"]["IHD__Date_of_intervention"],
            "IHD__Notes"=>$data["Questionnaire"]["IHD__Notes"],
                
            "Cardiomypathy__Cardiomypathy"=>$data["Questionnaire"]["Cardiomypathy__Cardiomypathy"],
            "Cardiomypathy__Diagnosis_Date"=>$data["Questionnaire"]["Cardiomypathy__Diagnosis_Date"],
            "Cardiomypathy__Notes"=>$data["Questionnaire"]["Cardiomypathy__Notes"],
                
            "CHF__CHF"=>$data["Questionnaire"]["CHF__CHF"],
            "CHF__LVEF"=>$data["Questionnaire"]["CHF__LVEF"],
            "CHF__Diagnosis_Date"=>$data["Questionnaire"]["CHF__Diagnosis_Date"],
            "CHF__Notes"=>$data["Questionnaire"]["CHF__Notes"],
                
            "Cor_pulmonale__Cor_pulmonale"=>$data["Questionnaire"]["Cor_pulmonale__Cor_pulmonale"],
            "Cor_pulmonale__Diagnosis_Date"=>$data["Questionnaire"]["Cor_pulmonale__Diagnosis_Date"],
            "Cor_pulmonale__Notes"=>$data["Questionnaire"]["Cor_pulmonale__Notes"],
                
            "Carotid_Artery_Stenosis__Stenosis"=>$data["Questionnaire"]["Carotid_Artery_Stenosis__Stenosis"],
            "Carotid_Artery_Stenosis__Diagnosis_Date"=>$data["Questionnaire"]["Carotid_Artery_Stenosis__Diagnosis_Date"],
            "Carotid_Artery_Stenosis__Notes"=>$data["Questionnaire"]["Carotid_Artery_Stenosis__Notes"],
                
            "Stroke__Stroke"=>$data["Questionnaire"]["Stroke__Stroke"],
            "Stroke__ICH"=>$data["Questionnaire"]["Stroke__ICH"],
            "Stroke__Cerebral_infarction"=>$data["Questionnaire"]["Stroke__Cerebral_infarction"],
            "Stroke__TIA"=>$data["Questionnaire"]["Stroke__TIA"],
            "Stroke__Diagnosis_Date"=>$data["Questionnaire"]["Stroke__Diagnosis_Date"],
            "Stroke__Notes"=>$data["Questionnaire"]["Stroke__Notes"],
                
            "Cancer__Cancer"=>$data["Questionnaire"]["Cancer__Cancer"],
            "Cancer__Diagnosis"=>$data["Questionnaire"]["Cancer__Diagnosis"],
            "Cancer__Pathology"=>$data["Questionnaire"]["Cancer__Pathology"],
            "Cancer__Diagnosis_Date"=>$data["Questionnaire"]["Cancer__Diagnosis_Date"],
            "Cancer__Notes"=>$data["Questionnaire"]["Cancer__Notes"],
                
            "Mental_disorder__Mental_disorder"=>$data["Questionnaire"]["Mental_disorder__Mental_disorder"],
            "Mental_disorder__Diagnosis"=>$data["Questionnaire"]["Mental_disorder__Diagnosis"],
            "Mental_disorder__Diagnosis_Date"=>$data["Questionnaire"]["Mental_disorder__Diagnosis_Date"],
            "Mental_disorder__Notes"=>$data["Questionnaire"]["Mental_disorder__Notes"],
                
            "COPD__COPD"=>$data["Questionnaire"]["COPD__COPD"],
            "COPD__GOLD_Stage"=>$data["Questionnaire"]["COPD__GOLD_Stage"],
            "COPD__Diagnosis_Date"=>$data["Questionnaire"]["COPD__Diagnosis_Date"],
            "COPD__Notes"=>$data["Questionnaire"]["COPD__Notes"],
            
            "Blood_test__Test_date1"=>$data["Questionnaire"]["Blood_test__Test_date1"],
            "Blood_test__Chol"=>$data["Questionnaire"]["Blood_test__Chol"],
            "Blood_test__LDL"=>$data["Questionnaire"]["Blood_test__LDL"],
            "Blood_test__HDL"=>$data["Questionnaire"]["Blood_test__HDL"],
            "Blood_test__Trigl"=>$data["Questionnaire"]["Blood_test__Trigl"],
            "Blood_test__Test_date2"=>$data["Questionnaire"]["Blood_test__Test_date2"],
            "Blood_test__HbA1c"=>$data["Questionnaire"]["Blood_test__HbA1c"],
            "Blood_test__FS"=>$data["Questionnaire"]["Blood_test__FS"],

            "Mortality__Death"=>$data["Questionnaire"]["Mortality__Death"],
            "Mortality__Direct_cause"=>$data["Questionnaire"]["Mortality__Direct_cause"],
            "Mortality__Antecedent_causes"=>$data["Questionnaire"]["Mortality__Antecedent_causes"],
            "Mortality__Date_of_death"=>$data["Questionnaire"]["Mortality__Date_of_death"],
            "Mortality__Notes"=>$data["Questionnaire"]["Mortality__Notes"],

            "OSA_related_disease__OSA_related_disease"=>$data["Questionnaire"]["OSA_related_disease__OSA_related_disease"],

            "Censering_date"=>$data["Questionnaire"]["Censering_date"],
            "Collecting_date"=>$data["Questionnaire"]["Collecting_date"],
            ],"Code = :Code",[":Code" => $data["Questionnaire"]["Code"]])){
                return true;
            }
            return false;
        }
        return FALSE;
    }
}

