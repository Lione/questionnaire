<?php
namespace app\controllers;
use Yii;
use app\models\Questionnaire;
use app\models\Manager;
use yii\web\Controller;
use yii\data\Pagination;
use moonland\phpexcel\Excel;

class IndexController extends Controller{
    public $layout = false;
    
    public function validate(){
        if(Yii::$app->session["admin"]["isLogin"]!="808"){
            Yii::$app->session->setFlash('info','[TimeOut]Please Login!!');
            return $this->redirect(["index/login"]);
        } else {
            return true;
        }
    }
    
    public function actionLogin(){
        $model = new Manager();
        if(Yii::$app->request->isPost){
            $post = Yii::$app->request->post();
            if($model->login($post)){
                Yii::$app->session->setFlash('info','Login success');
                return $this->redirect(["index/manage"]);
            } else {
                Yii::$app->session->setFlash('info','Login error');
                return $this->render("login",["model"=>$model]);
            }
        }
        return $this->render("login",["model"=>$model]);
    }

    public function actionQuestionnaire(){
        $model = new Questionnaire();
        if(Yii::$app->request->isPost){
            $post = Yii::$app->request->post();
            if($model->submit($post)){
                Yii::$app->session->setFlash("info","Submit success");
                return $this->redirect(["index/questionnaire"]);
            }else{
                Yii::$app->session->setFlash("info","Submit error,Code/ID was blank!");
                return $this->render("show",["post"=>$post]);
            }
        }
        return $this->render("table",["model" => $model]);
    }

    public function actionManage(){
        $this->validate();
        $model = Questionnaire::find()->orderBy(["Code"=>SORT_ASC]);
        $count = $model->count();
        $pager = new Pagination(['totalCount' => $count, 'pageSize' => 100]);
        $lists = $model->offset($pager->offset)->limit($pager->limit)->all();
        return $this->render("manager", ['lists' => $lists, 'pager' => $pager]);
    }
    
    public function actionSearch(){
        $this->validate();
        if(Yii::$app->request->isPost){
            $post = Yii::$app->request->post();
        } else {
            $this->actionManage();
        }
        $model = Questionnaire::find()->where("Code = :Code or ID = :ID",[":Code"=>$post["search"],":ID"=>$post["search"]])->orderBy(["Code"=>SORT_ASC]);
        $count = $model->count();
        $pager = new Pagination(['totalCount' => $count, 'pageSize' => 10]);
        $lists = $model->offset($pager->offset)->limit($pager->limit)->all();
        return $this->render("manager", ['lists' => $lists, 'pager' => $pager]);
    }
    
    public function actionChangepass(){
        $this->validate();
        $model = new Manager();
        if(Yii::$app->request->isPost){
            $post = Yii::$app->request->post();
            if($model->change($post)){
                Yii::$app->session->setFlash('info','Change successfully');
                Yii::$app->session["admin"]=[
                    "isLogin"=>"0"
                ];
                return $this->redirect(["index/login"]);
            } else {
                Yii::$app->session->setFlash('info','Change false');
            }
        }
        return $this->render("changepass",['model'=>$model]);
    }
    
    public function actionExport(){
        $this->validate();
        $model = Questionnaire::find()->orderBy(["Code"=>SORT_ASC]);
        $data = $model->all();
        Excel::export([
            'models'=>$data,
            'format'=>'Excel5',
            'fileName'=>  date("d-m-Y H:i:s"),
            'columns'=>[

                "Code",
                "ID",

                "OSA__Study_notes",
                "OSA__Study_involvement",
                "OSA__Diagnosis_Date",
                "OSA__ESS",
                "OSA__RDI",
                "OSA__TST",
                "OSA__Eff",
                "OSA__CPAP_titration",
                "OSA__CPAP_pressure",
                "OSA__CPAP_Compliance",
                "OSA__OA",
                "OSA__OA_compliance",
                "OSA__Others",

                "HTN__HTN",
                "HTN__Diagnosis_Date",
                "HTN__Medication",
                "HTN__Num_of_Antihypertensive_Agents",
                "HTN__Med1",
                "HTN__Med2",
                "HTN__Med3",
                "HTN__Med4",
                "HTN__Med5",
                "HTN__Notes",

                "DM__DM",
                "DM__IGT",
                "DM__IFG",
                "DM__Diagnosis_Date",
                "DM__Notes",

                "Hyperlipidaemia__Lipid",
                "Hyperlipidaemia__Diagnosis_Date",
                "Hyperlipidaemia__Med",
                "Hyperlipidaemia__Date_of_treatment",
                "Hyperlipidaemia__Notes",

                "NAFLD__NAFLD",
                "NAFLD__Diagnosis_Date",
                "NAFLD__Notes",

                "Metabolic_syndrome__MS",
                "Metabolic_syndrome__Diagnosis_Date",
                "Metabolic_syndrome__Notes",

                "Dysrrhythmias__Dysrrhythmias",
                "Dysrrhythmias__Atrial_fibrillation",
                "Dysrrhythmias__Diagnosis_Date",
                "Dysrrhythmias__Notes",

                "IHD__IHD",
                "IHD__AMI",
                "IHD__STEMI",
                "IHD__NSTEMI",
                "IHD__Diagnosis_Date",
                "IHD__Unstable_angina",
                "IHD__CABG",
                "IHD__PCI",
                "IHD__Date_of_intervention",
                "IHD__Notes",

                "Cardiomypathy__Cardiomypathy",
                "Cardiomypathy__Diagnosis_Date",
                "Cardiomypathy__Notes",

                "CHF__CHF",
                "CHF__LVEF",
                "CHF__Diagnosis_Date",
                "CHF__Notes",

                "Cor_pulmonale__Cor_pulmonale",
                "Cor_pulmonale__Diagnosis_Date",
                "Cor_pulmonale__Notes",

                "Carotid_Artery_Stenosis__Stenosis",
                "Carotid_Artery_Stenosis__Diagnosis_Date",
                "Carotid_Artery_Stenosis__Notes",

                "Stroke__Stroke",
                "Stroke__ICH",
                "Stroke__Cerebral_infarction",
                "Stroke__TIA",
                "Stroke__Diagnosis_Date",
                "Stroke__Notes",

                "Cancer__Cancer",
                "Cancer__Diagnosis",
                "Cancer__Pathology",
                "Cancer__Diagnosis_Date",
                "Cancer__Notes",

                "Mental_disorder__Mental_disorder",
                "Mental_disorder__Diagnosis",
                "Mental_disorder__Diagnosis_Date",
                "Mental_disorder__Notes",

                "COPD__COPD",
                "COPD__GOLD_Stage",
                "COPD__Diagnosis_Date",
                "COPD__Notes",

                "Blood_test__Test_date1",
                "Blood_test__Chol",
                "Blood_test__LDL",
                "Blood_test__HDL",
                "Blood_test__Trigl",
                "Blood_test__Test_date2",
                "Blood_test__HbA1c",
                "Blood_test__FS",

                "Mortality__Death",
                "Mortality__Direct_cause",
                "Mortality__Antecedent_causes",
                "Mortality__Date_of_death",

                "OSA_related_disease__OSA_related_disease",

                "Censering_date",
                "Collecting_date",

            ],
        'headers'=>[

            "Code"=>"Code",
            "ID"=>"ID",

            "OSA__Study_notes"=>"Study notes",
            "OSA__Study_involvement"=>"Study involvement",
            "OSA__Diagnosis_Date"=>"Diagnosis Date",
            "OSA__ESS"=>"ESS",
            "OSA__RDI"=>"RDI",
            "OSA__TST"=>"TST",
            "OSA__Eff"=>"Eff%",
            "OSA__CPAP_titration"=>"CPAP titration",
            "OSA__CPAP_pressure"=>"CPAP pressure",
            "OSA__CPAP_Comliance"=>"CPAP Compliance",
            "OSA__OA"=>"OA",
            "OSA__OA_compliance"=>"OA compliance",
            "OSA__Others"=>"Others",

            "HTN__HTN"=>"HTN",
            "HTN__Diagnosis_Date"=>"Diagnosis Date",
            "HTN__Medication"=>"Medication",
            "HTN__Num_of_Antihypertensive_Agents"=>"Num. of Antihypertensive Agents",
            "HTN__Med1"=>"Med1",
            "HTN__Med2"=>"Med2",
            "HTN__Med3"=>"Med3",
            "HTN__Med4"=>"Med4",
            "HTN__Med5"=>"Med5",
            "HTN__Notes"=>"Notes",

            "DM__DM"=>"DM",
            "DM__IGT"=>"IGT",
            "DM__IFG"=>"IFG",
            "DM__Diagnosis_Date"=>"Diagnosis Date",
            "DM__Notes"=>"Notes",

            "Hyperlipidaemia__Lipid"=>"Lipid",
            "Hyperlipidaemia__Diagnosis_Date"=>"Diagnosis Date",
            "Hyperlipidaemia__Med"=>"Med",
            "Hyperlipidaemia__Date_of_treatment"=>"Date of treatment",
            "Hyperlipidaemia__"=>"Notes",

            "NAFLD__NAFLD"=>"NAFLD",
            "NAFLD__Diagnosis_Date"=>"Diagnosis Date",
            "NAFLD__Notes"=>"Notes",

            "Metabolic_syndrome__MS"=>"MS",
            "Metabolic_syndrome__Diagnosis_Date"=>"Diagnosis Date",
            "Metabolic_syndrome__Notes"=>"Notes",

            "Dysrrhythmias__Dysrrhythmias"=>"Dysrrhythmias",
            "Dysrrhythmias__Atrial_fibrillation"=>"Atrial fibrillation",
            "Dysrrhythmias__Diagnosis_Date"=>"Diagnosis Date",
            "Dysrrhythmias__Notes"=>"Notes",

            "IHD__IHD"=>"IHD",
            "IHD__AMI"=>"AMI",
            "IHD__STEMI"=>"STEMI",
            "IHD__NSTEMI"=>"NSTEMI",
            "IHD__Diagnosis_Date"=>"Diagnosis Date",
            "IHD__Unstable_angina"=>"Unstable angina",
            "IHD__CABG"=>"CABG",
            "IHD__PCI"=>"PCI",
            "IHD__Date_of_intervention"=>"Date of intervention",
            "IHD__Notes"=>"Notes",

            "Cardiomypathy__Cardiomypathy"=>"Cardiomypathy",
            "Cardiomypathy__Diagnosis_Date"=>"Diagnosis Date",
            "Cardiomypathy__Notes"=>"Notes",

            "CHF__CHF"=>"CHF",
            "CHF__LVEF"=>"LVEF%",
            "CHF__Diagnosis_Date"=>"Diagnosis Date",
            "CHF__Notes"=>"Notes",

            "Cor_pulmonale__Cor_pulmonale"=>"Cor pulmonale",
            "Cor_pulmonale__Diagnosis_Date"=>"Diagnosis Date",
            "Cor_pulmonale__Notes"=>"Notes",

            "Carotid_Artery_Stenosis__Stenosis"=>"Stenosis",
            "Carotid_Artery_Stenosis__Diagnosis_Date"=>"Diagnosis Date",
            "Carotid_Artery_Stenosis__Notes"=>"Notes",

            "Stroke__Stroke"=>"Stroke",
            "Stroke__ICH"=>"ICH",
            "Stroke__Cerebral_infarction"=>"Cerebral infarction",
            "Stroke__TIA"=>"TIA",
            "Stroke__Diagnosis_Date"=>"Diagnosis Date",
            "Stroke__Notes"=>"Notes",

            "Cancer__Cancer"=>"Cancer",
            "Cancer__Diagnosis"=>"Diagnosis",
            "Cancer__Pathology"=>"Pathology",
            "Cancer__Diagnosis_Date"=>"Diagnosis Date",
            "Cancer__Notes"=>"Notes",

            "Mental_disorder__Mental_disorder"=>"Mental disorder",
            "Mental_disorder__Diagnosis"=>"Diagnosis",
            "Mental_disorder__Diagnosis_Date"=>"Diagnosis Date",
            "Mental_disorder__Notes"=>"Notes",

            "COPD__COPD"=>"COPD",
            "COPD__GOLD_Stage"=>"GOLD Stage",
            "COPD__Diagnosis_Date"=>"Diagnosis Date",
            "COPD__Notes"=>"Notes",

            "Blood_test__Test_date1"=>"Test date",
            "Blood_test__Chol"=>"Chol	",
            "Blood_test__LDL"=>"LDL",
            "Blood_test__HDL"=>"HDL",
            "Blood_test__Trigl"=>"Trigl",
            "Blood_test__Test_date2"=>"Test date",
            "Blood_test__HbA1c"=>"HbA1c",
            "Blood_test__FS"=>"FS",

            "Mortality__Death"=>"Death",
            "Mortality__Direct_cause"=>"Direct cause",
            "Mortality__Antecedent_causes"=>"Antecedent causes",
            "Mortality__Date_of_death"=>"Date of death",
            "Mortality__Notes"=>"Notes",

            "OSA_related_disease__OSA_related_disease"=>"OSA-related disease",

            "Censering_date"=>"Censering date",
            "Collecting_date"=>"Collecting date",
        ]
        ]);
    }
    
    public function actionView(){
        $this->validate();
        $model = new Questionnaire();
        $data = $model->find()->where("Code = :Code",[":Code"=>Yii::$app->request->get()[1]["Code"]])->one();
        return $this->render("view",["model"=>$model,"data"=>$data]);
    }   
    
    public function actionEdit(){
        $this->validate();
        $model = new Questionnaire();
        if(Yii::$app->request->isPost){
            $post = Yii::$app->request->post();
            if($model->modelUpdate($post)){
                Yii::$app->session->setFlash('info','Update Successful');
                return $this->redirect(["index/manage"]);
            } else {
                Yii::$app->session->setFlash('info','Update Faile');
            }
        }
        $data = $model->find()->where("Code = :Code",[":Code"=>Yii::$app->request->get()[1]["Code"]])->one();
        return $this->render("edit",["model"=>$model,"data"=>$data]);
    }
    
    public function actionDel(){
        $this->validate();
        $model = new Questionnaire();
        if($model->deleteAll("Code = :Code",[":Code"=>Yii::$app->request->get()[1]["Code"]])){
            Yii::$app->session->setFlash('info','Delete Code:'.Yii::$app->request->get()[1]["Code"].' Successful');
            return $this->redirect(["index/manage"]);
        } else {
            Yii::$app->session->setFlash('info','Delete False!');
            return $this->redirect(["index/manage"]);
        }
    }
    
    public function actionDelall(){
        $this->validate();
        $model = new Questionnaire();
        $model->deleteAll();
    }
    
    public function actionLogout(){
        Yii::$app->session->removeAll();
        return $this->redirect(["index/login"]);
    }
    
    public function actionTest(){
        $this->validate();
        $model = new Questionnaire();
        for($i=0;$i<100;$i++){
        $model->Code = $i+2000;
        $model->ID = $i+1000;
        $model->OSA__Study_notes = "test".$i;
        $model->OSA__Study_involvement = "test".$i;
        $model->OSA__Diagnosis_Date = "test".$i;
        $model->OSA__ESS = "test".$i;
        $model->OSA__RDI = "test".$i;
        $model->OSA__CPAP_pressure = "test".$i;
        $model->OSA__TST = "test".$i;
        $model->OSA__Eff = "test".$i;
        $model->OSA__CPAP_titration = "test".$i;
        $model->OSA__CPAP_Compliance = "test".$i;
        $model->OSA__OA = "test".$i;
        $model->OSA__OA_compliance = "test".$i;
        $model->OSA__Others = "test".$i;
        $model->HTN__HTN = "test".$i;
        $model->HTN__Diagnosis_Date = "test".$i;
        $model->HTN__Medication = "test".$i;
        $model->HTN__Num_of_Antihypertensive_Agents = "test".$i;
        $model->HTN__Med1 = "test".$i;
        $model->HTN__Med2 = "test".$i;
        $model->HTN__Med3 = "test".$i;
        $model->HTN__Med4 = "test".$i;
        $model->HTN__Med5 = "test".$i;
        $model->HTN__Notes = "test".$i;
        
        $model->DM__DM = "test".$i;
        $model->DM__IGT = "test".$i;
        $model->DM__IFG = "test".$i;
        $model->DM__Diagnosis_Date = "test".$i;
        $model->DM__Notes = "test".$i;
        
        $model->Hyperlipidaemia__Lipid = "test".$i;
        $model->Hyperlipidaemia__Diagnosis_Date = "test".$i;
        $model->Hyperlipidaemia__Med = "test".$i;
        $model->Hyperlipidaemia__Date_of_treatment = "test".$i;
        $model->Hyperlipidaemia__Notes = "test".$i;
        
        $model->NAFLD__NAFLD = "test".$i;
        $model->NAFLD__Diagnosis_Date = "test".$i;
        $model->NAFLD__Notes = "test".$i;
        
        $model->Metabolic_syndrome__MS = "test".$i;
        $model->Metabolic_syndrome__Diagnosis_Date = "test".$i;
        $model->Metabolic_syndrome__Notes = "test".$i;
        
        $model->Dysrrhythmias__Dysrrhythmias = "test".$i;
        $model->Dysrrhythmias__Atrial_fibrillation = "test".$i;
        $model->Dysrrhythmias__Diagnosis_Date = "test".$i;
        $model->Dysrrhythmias__Notes = "test".$i;
        
        $model->IHD__IHD = "test".$i;
        $model->IHD__AMI = "test".$i;
        $model->IHD__STEMI = "test".$i;
        $model->IHD__NSTEMI = "test".$i;
        $model->IHD__Unstable_angina = "test".$i;
        $model->IHD__Diagnosis_Date = "test".$i;
        $model->IHD__CABG = "test".$i;
        $model->IHD__PCI = "test".$i;
        $model->IHD__Date_of_intervention = "test".$i;
        $model->IHD__Notes = "test".$i;
        
        $model->Cardiomypathy__Cardiomypathy = "test".$i;
        $model->Cardiomypathy__Diagnosis_Date = "test".$i;
        $model->Cardiomypathy__Notes = "test".$i;
        
        $model->CHF__CHF = "test".$i;
        $model->CHF__LVEF = "test".$i;
        $model->CHF__Diagnosis_Date = "test".$i;
        $model->CHF__Notes = "test".$i;
        
        $model->Cor_pulmonale__Cor_pulmonale = "test".$i;
        $model->Cor_pulmonale__Diagnosis_Date = "test".$i;
        $model->Cor_pulmonale__Notes = "test".$i;
        
        $model->Carotid_Artery_Stenosis__Stenosis = "test".$i;
        $model->Carotid_Artery_Stenosis__Diagnosis_Date = "test".$i;
        $model->Carotid_Artery_Stenosis__Notes = "test".$i;
        
        $model->Stroke__Stroke = "test".$i;
        $model->Stroke__ICH = "test".$i;
        $model->Stroke__Cerebral_infarction = "test".$i;
        $model->Stroke__TIA = "test".$i;
        $model->Stroke__Diagnosis_Date = "test".$i;
        $model->Stroke__Notes = "test".$i;
        
        $model->Cancer__Cancer = "test".$i;
        $model->Cancer__Diagnosis = "test".$i;
        $model->Cancer__Pathology = "test".$i;
        $model->Cancer__Diagnosis_Date = "test".$i;
        $model->Cancer__Notes = "test".$i;
        
        $model->Mental_disorder__Mental_disorder = "test".$i;
        $model->Mental_disorder__Diagnosis = "test".$i;
        $model->Mental_disorder__Diagnosis_Date = "test".$i;
        $model->Mental_disorder__Notes = "test".$i;
        
        $model->COPD__COPD = "test".$i;
        $model->COPD__GOLD_Stage = "test".$i;
        $model->COPD__Diagnosis_Date = "test".$i;
        $model->COPD__Notes = "test".$i;
        
        $model->Blood_test__Test_date1 = "test".$i;
        $model->Blood_test__Chol = "test".$i;
        $model->Blood_test__LDL = "test".$i;
        $model->Blood_test__HDL = "test".$i;
        $model->Blood_test__Trigl = "test".$i;
        $model->Blood_test__Test_date2 = "test".$i;
        $model->Blood_test__HbA1c = "test".$i;
        $model->Blood_test__FS = "test".$i;
        $model->Mortality__Death = "test".$i;
        $model->Mortality__Direct_cause = "test".$i;
        $model->Mortality__Antecedent_causes = "test".$i;
        $model->Mortality__Date_of_death = "test".$i;
        $model->Mortality__Notes = "test".$i;
        $model->OSA_related_disease__OSA_related_disease = "test".$i;
        $model->Censering_date = "test".$i;
        $model->Collecting_date = "test".$i;
        $model->insert();
        }
    }

}
